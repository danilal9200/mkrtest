<?php

use \PHPUnit\Framework\TestCase;

include 'FitnessCenter.php';

class FitnessCenterTest extends TestCase
{

    private $fitness_center;

    protected function setUp(): void
    {
        $this->fitness_center = new FitnessCenter();
    }

    public function testClient(){
        $clients =$this->fitness_center->getClientsWithYearsInFitness(5);

        $this->assertEquals(5, count($clients));
    }


    public function testSurnameCount($surname, $excepted){
        $result =$this->fitness_center->getClientsCountBySurname($surname);
        $this->assertEquals($excepted, $result);

    }

    public function addDataProvider(){
        return array(
            array('Lopata', 3),
            array('Lopata', 5),
            array('Telka', 1),

        );
    }
    protected function tearDown() : void
    {
        isset($this->fitness_center);
    }
}