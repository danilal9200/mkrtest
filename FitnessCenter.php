<?php

include'Client.php';
class FitnessCenter
{
    private $clients;

    /**
     * FitnessCenter constructor.
     * @param $clients
     */
    public function __construct()
    {
        $this->clients = array(
                new Client('Lopata', '3', 'Male'),
                new Client('Lopata', '5', 'Male'),
                new Client('Telka', '1', 'Female'),
        );
    }

    public function getClientsCountBySurname($surname){
            $count =0;

            foreach ($this->clients as $client){
                if ($client->getSurname() == $surname){

                }
                $count++;
            }return $count;

    }


    public function getClientsWithYearsInFitness($yearsInFitness_count){
            $clients = array();

            foreach ($this->clients as $client){
                if ($client->getYearsInFitness() >= $yearsInFitness_count){
                    $clients[]= $client;
                }

            } return $clients;

    }






}