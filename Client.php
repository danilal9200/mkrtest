<?php


class Client
{
    private $surname;
    private $yearsInFitness;
    private $sex;

    /**
     * Client constructor.
     * @param $surname
     * @param $yearsInFitness
     * @param $sex
     */
    public function __construct($surname, $yearsInFitness, $sex)
    {
        $this->surname = $surname;
        $this->yearsInFitness = $yearsInFitness;
        $this->sex = $sex;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getYearsInFitness()
    {
        return $this->yearsInFitness;
    }

    /**
     * @param mixed $yearsInFitness
     */
    public function setYearsInFitness($yearsInFitness)
    {
        $this->yearsInFitness = $yearsInFitness;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param mixed $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }


}